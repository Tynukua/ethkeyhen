module util.colored;
import std.stdio;

/// ESC sequence for colors
const string ESC = "\x1b[";
const string RED = ESC ~ "31m";
const string GREEN = ESC ~ "32m";
const string YELLOW = ESC ~ "33m";
const string BLUE = ESC ~ "34m";
const string MAGENTA = ESC ~ "35m";
const string CYAN = ESC ~ "36m";
const string RESET = ESC ~ "0m";

/// ESC sequence for bold colors
const string BOLD = ESC ~ "1m";
const string BRED = BOLD ~ RED;
const string BGREEN = BOLD ~ GREEN;
const string BYELLOW = BOLD ~ YELLOW;
const string BBLUE = BOLD ~ BLUE;
const string BMAGENTA = BOLD ~ MAGENTA;
const string BCYAN = BOLD ~ CYAN;
const string BWHITE = BOLD ~ RESET;

/// creates colored string with checks offset and length
string colored(string s, string color, size_t offset = 0, size_t length = size_t.max)
in (offset >= 0)
{

    if (length == size_t.max)
    {
        length = s.length;
    }
    auto end = offset + length;
    offset = (offset > s.length ? s.length : offset);
    end = (end > s.length ? s.length : end);
    return s[0 .. offset] ~ color ~ s[offset .. end] ~ RESET ~ s[end .. s.length];
}

/// unittest for colored function
unittest
{
    assert(colored("hello", RED, 0, 5) == RED ~ "hello" ~ RESET);
    assert(colored("hello", RED, 0) == RED ~ "hello" ~ RESET);
    assert(colored("hello", RED, 0, 10) == RED ~ "hello" ~ RESET);

    assert(colored("hello", RED, 2, 5) == "he" ~ RED ~ "llo" ~ RESET);
    assert(colored("hello", RED, 2, 10) == "he" ~ RED ~ "llo" ~ RESET);
    assert(colored("hello", RED, 2) == "he" ~ RED ~ "llo" ~ RESET);
}

import secp256k1 : secp256k1;
import std.regex : regex, matchFirst;

void printAddress(RegEx)(ubyte[20 + 32] buf, bool printSecKey, RegEx r, bool check)
{
    import std.digest : toHexString;

    string address = buf[32 .. $].toHexString;
    string secKey = buf[0 .. 32].toHexString;
    string result = "";
    if (!r.empty)
    {
        auto m = matchFirst(address, r);
        if (!m)
        {
            return;
        }
        result ~= m.pre ~ colored(m.hit, BRED) ~ m.post;
    }
    else
    {
        result ~= address;
    }
    if (check)
    {
        auto sk = new secp256k1(buf[0 .. 32]);
        if (sk.address != buf[32 .. $])
        {
            return;
        }
    }
    if (printSecKey)
    {
        result ~= " " ~ secKey;
    }
    result.writeln;
}

unittest
{
    printAddress((new secp256k1).secKey, true, regex("D"));
}
