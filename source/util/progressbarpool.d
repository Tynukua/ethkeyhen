module util.progressbarpool;

struct ProgressBarPool
{
    import std.stdio;
    import std.conv : to;

    /// Length of progressbars
    ubyte len = 24;
    float[] progresses;
    string lBorder = "[";
    string rBorder = "]";
    string voidContent = "-";
    string fullContent = "=";
    bool isFirstDraw = true;
    this(float[] ps)
    {
        progresses = ps;
        this.len = len;
    }

    void draw(bool clean = true)
    {
        if (clean && !isFirstDraw){
            writefln!"\033[%dF"(progresses.length + 1);
        }

        foreach (progress; progresses)
        {
            string fill = "";
            ubyte progresLen = (len * progress).to!ubyte;
            foreach (i; 0 .. len)
            {
                if (i <= progresLen)
                    fill ~= fullContent;
                else
                    fill ~= voidContent;
            }
            writefln!"%4d%s%s%s "(cast(int)(progress * 100), lBorder, fill, rBorder);
        }
        isFirstDraw = false;
    }
}

unittest
{
    import core.thread;
    import util.intsig;
    import std.stdio;
    import std.random;

    auto bars = ProgressBarPool([0f, 0.2f, 0.99f, 1.00001f]);
    bool breaked = false;
    bars.draw(false);
    foreach (i; 0 .. 100)
    {
        foreach (ref p; bars.progresses)
        {
            p = uniform01!float;
        }
        bars.draw;
        Thread.sleep(100.dur!"msecs");
        if (isInterupted)
        {
            breaked = true;
            bars.draw;
            break;
        }
    }
    if (breaked)
        "cancelled".writeln;
    else
        "done".writeln;
}
