module util.intsig;

import core.atomic;

import core.stdc.signal : signal, SIGINT;

private shared interupted = false;

extern (C) void intsig_handler(int _) nothrow @nogc @system
{
    isInterupted = true;
}

static this()
{
    signal(SIGINT, &intsig_handler);
}

/// getter for interupted
bool isInterupted() nothrow @nogc @system
{
    return interupted.atomicLoad;
}

/// setter for interupted
void isInterupted(bool flag) nothrow @nogc @system
{
    interupted.atomicStore = flag;
}

unittest
{
    SIGINT.intsig_handler;
    assert(isInterupted);
    isInterupted = false;
    assert(!isInterupted);
}
