module sub.generating;

import secp256k1 : secp256k1;
import std.stdio : File, writeln;
import std.conv : to;
import std.exception : enforce;
import std.algorithm : canFind;
import core.thread : Thread, dur, ThreadGroup;
import util.intsig;

static immutable BUFFER_SIZE = 1 << 8;

alias Chunk = ubyte[32 + 20][BUFFER_SIZE];

static immutable sizeKB = (Chunk.sizeof / 1024).to!string;

class GenThread : Thread
{
    import core.sync.mutex;

    ulong count;
    ulong i;
    WriterThread writer;

    this(ulong count, WriterThread wt)
    {
        this.count = count;
        writer = wt;
        super(&run);
    }

    void run()
    {
        Chunk buf;
        foreach (i; 0 .. count)
        {
            this.i = i;
            foreach (ref keyData; buf)
            {
                secp256k1 key;
                ubyte[20] addr;
                do
                {
                    key = new secp256k1;
                    addr = key.address;
                    keyData = key.secKey ~ addr;
                }
                while (!addr[].canFind(0));
            }
            writer.qMutex.lock;
            writer.q.insertBack(buf);
            writer.qMutex.unlock;
            if (isInterupted)
                break;
        }
    }
}

class WriterThread : Thread
{
    import std.container : DList;

    import core.sync.mutex;

    DList!Chunk q;
    Mutex qMutex;
    File f;
    bool delegate() isWorking;
    this(string fileName, bool delegate() isw)
    {
        f = fileName.File("ab");
        qMutex = new Mutex;
        isWorking = isw;
        super(&run);
    }

    void run()
    {
        while (isWorking())
            while (!q.empty)
            {
                qMutex.lock;
                auto chunk = q.front;
                q.removeFront;
                qMutex.unlock;
                f.rawWrite(chunk);
                if (isInterupted)
                    break;
            }
        f.detach;
    }

}

bool isAlive(ThreadGroup grp)
{
    foreach (t; grp)
    {
        if (t.isRunning)
            return true;
    }
    return false;
}

void startGenerating(string[] argv)
{
    import util.    progressbarpool;
    import std.getopt;
    import core.sync.mutex : Mutex;
    import core.cpuid : threadsPerCPU;

    ulong threadCount = threadsPerCPU;
    string fileName = "sec";
    ulong count = 1 << 9;

    auto optRes = argv.getopt("c|count",
            "Count of chunk(" ~ sizeKB ~ "KB) generating default: " ~ count.to!string, &count,
            "o", "File to output addresses and keys default: " ~ fileName, &fileName, "j",
            "Thread count default: " ~ threadsPerCPU.to!string, &threadCount);
    if (optRes.helpWanted)
    {
        defaultGetoptPrinter("ethkeygen gen [OPTIONS ...]", optRes.options);
        return;
    }

    writeln(threadCount, " threads are used");

    auto group = new ThreadGroup;
    auto writer = new WriterThread(fileName, { return group.isAlive; });
    foreach (i; 0 .. threadCount)
        group.add(new GenThread(count, writer));
    foreach (t; group)
        t.start;
    writer.start;
    auto bar = ProgressBarPool(new float[threadCount]);
    void actulizeBar()
    {
        ulong i = 0;
        foreach (t; group)
        {
            auto thread = cast(GenThread) t;
            bar.progresses[i++] = (thread.i + 1) / cast(float) thread.count;
        }
    }

    while (writer.isRunning)
    {
        actulizeBar();
        bar.draw;
        Thread.sleep(500.dur!"msecs");
    }

    actulizeBar();
    bar.draw();
}
