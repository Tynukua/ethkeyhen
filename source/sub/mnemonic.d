module sub.mnemonic;
import std.stdio : writefln, writeln, File;

void startMnemonic(string[] argv)
{
    import std.getopt : getopt, defaultGetoptPrinter;
    import std.digest : toHexString;
    import std.regex : regex, matchFirst;

    string regexFilter = "";
    bool printSecKey = false;
    string mnemonic = "Std Mnemonic";
    int count = 10;
    int offset = 0;
    auto optRes = argv.getopt("s|sec", "Print sec key with private address",
            &printSecKey, "r|regex", "Regex filter for address", &regexFilter,
            "m|mnemonic", "Mnemonic to generated", &mnemonic, "c|count",
            "Count to generated adresses", &count, "o|offset", "offset for generating", &offset);
    if (optRes.helpWanted)
    {
        defaultGetoptPrinter("ethkeygen  mnemonic [OPTIONS ...]", optRes.options);
        return;
    }

    foreach (i; offset .. offset + count)
    {
        auto sk = createAccount(cast(ubyte[]) mnemonic, i);
        import util.colored : printAddress;
        printAddress((sk.address ~ sk.secKey)[0..52],printSecKey,regex(regexFilter),false);
    }
}

auto createAccount(ubyte[] m, int i)
{
    import keccak : keccak256;
    import secp256k1 : secp256k1;
    import std.bitmanip : nativeToBigEndian;

    auto sk = new secp256k1(keccak256(m ~ i.nativeToBigEndian));
    return sk;
}
