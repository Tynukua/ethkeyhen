module sub.loading;

void startLoading(string[] argv)
{
    import std.getopt : getopt, defaultGetoptPrinter;
    import std.stdio : writefln, writeln, File;
    import std.digest : toHexString;
    import std.regex : regex, matchFirst;
    import util.intsig;
    import util.colored : printAddress;
    import core.cpuid : threadsPerCPU;

    string regexFilter = "";
    string fileName = "sec";
    bool checkAddress = false;
    bool printSecKey = false;
    int treadCount = threadsPerCPU;
    //   treadCount = (treadCount > 4 ? 4 : treadCount);
    auto optRes = argv.getopt("f|file", "File to load addresses", &fileName,
            "s|sec", "Print sec key with private address", &printSecKey,
            "check", "Check printed keys", &checkAddress, "r|regex",
            "Regex filter for address", &regexFilter);
    if (optRes.helpWanted)
    {
        defaultGetoptPrinter("ethkeygen load [OPTIONS ...]", optRes.options);
        return;
    }

    auto r = regex(regexFilter);
    void printer(ubyte[32 + 20] b)
    {
        printAddress(b, printSecKey, r, checkAddress);
    }

    File f = fileName;
    scope (exit)
        f.close;
    auto addrCount = f.size / 52;
    auto chunkSize = addrCount / treadCount * 52;
    auto lastChunkSize = addrCount % treadCount * 52;
    Reader[] readers = new Reader[treadCount];
    foreach (i, ref reader; readers)
    {
        reader = new Reader(fileName, i * chunkSize, chunkSize, &printer);
    }
    readers[$ - 1].length += lastChunkSize;
    foreach (reader; readers)
    {
        reader.start;
        reader.join;
    }
}

import core.thread : Thread, ThreadGroup;

class Reader : Thread
{
    string fileName;
    size_t offset;
    size_t length;
    void delegate(ubyte[32 + 20]) print;

    this(string fileName, size_t offset, size_t length, void delegate(ubyte[32 + 20]) printer)
    {
        this.fileName = fileName;
        this.offset = offset;
        this.length = length;
        this.print = printer;
        super(&run);
    }

    void run()
    {
        import std.stdio;
        import util.intsig;

        File f = fileName;
        f.seek(offset);
        ubyte[52 * 1024] buf;
        ubyte[52] address;
        size_t end = offset + length;

        while (f.tell < end && !isInterupted)
        {
            auto endLocal = f.tell + buf.sizeof;
            size_t count = 1024;
            if (endLocal > end)
                count = (end - f.tell) / 52;
            f.rawRead(buf);
            foreach (i; 0 .. count)
            {
                address[] = buf[i * 52 .. i * 52 + 52];
                print(address);
            }
        }
    }
}
