import std.stdio;
import sub.generating;
import sub.mnemonic;

import sub.loading;
import std.digest : toHexString;

void main(string[] argv)
{
    argv = argv[1 .. $];
    string cmd = (argv.length > 0 ? argv[0] : "");
    switch (cmd)
    {
    case "load":
        argv.startLoading;
        break;
    case "gen":
        argv.startGenerating;
        break;
    case "mnemonic":
        argv.startMnemonic;
        break;
    default:
        `Commands:
    gen
    load
    mnemonic`.writeln;
        break;
    }
}
