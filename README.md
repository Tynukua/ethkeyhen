# ethkeygen

program to generating and filtering ethereum adresses

## Generating

```
ethkeygen gen 
# Generating pk into sec file 
#   ethkeygen gen [OPTIONS ...]
#   -c --count Count of chunk(13KB) generating default: 512
#   -o         File to output addresses and keys default: sec
#   -j         Thread count default: 8
#
```

## Reading

```
ethkeygen -f sec -r 213 
#   ethkeygen load [OPTIONS ...]
#   -f  --file File to load addresses
#   -s   --sec Print sec key with private address
#      --check Check printed keys
#   -r --regex Regex filter for address
#   -h  --help This help information.
```
